package ru.helmetoff.tm.model;

import ru.helmetoff.tm.constant.ITerminalConst;

public class TerminalCommand {

    public static final TerminalCommand HELP = new TerminalCommand(
            ITerminalConst.CMD_HELP, ITerminalConst.ARG_HELP, ITerminalConst.HELP_DESCRIPTION
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            ITerminalConst.CMD_ABOUT, ITerminalConst.ARG_ABOUT, ITerminalConst.ABOUT_DESCRIPTION
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            ITerminalConst.CMD_VERSION, ITerminalConst.ARG_VERSION, ITerminalConst.VERSION_DESCRIPTION
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            ITerminalConst.CMD_INFO, ITerminalConst.ARG_INFO, ITerminalConst.INFO_DESCRIPTION
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            ITerminalConst.CMD_EXIT, null, ITerminalConst.EXIT_DESCRIPTION
    );

    private String name = "";

    private String arg = "";

    private String description = "";

    public TerminalCommand() {
    }

    public TerminalCommand(String name) {
        this.name = name;
    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(" - ").append(description);
        return result.toString();
    }
}

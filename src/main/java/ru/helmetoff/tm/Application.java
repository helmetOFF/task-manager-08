package ru.helmetoff.tm;

import ru.helmetoff.tm.constant.IApplicationInfo;
import ru.helmetoff.tm.constant.ITerminalConst;
import ru.helmetoff.tm.model.TerminalCommand;
import ru.helmetoff.tm.util.INumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case ITerminalConst.CMD_HELP:
                displayHelp();
                break;
            case ITerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case ITerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case ITerminalConst.CMD_INFO:
                displayInfo();
                break;
            case ITerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                displayWrongCommand(command);
                break;
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ITerminalConst.ARG_HELP:
                displayHelp();
                break;
            case ITerminalConst.ARG_VERSION:
                displayVersion();
                break;
            case ITerminalConst.ARG_ABOUT:
                displayAbout();
                break;
            case ITerminalConst.ARG_INFO:
                displayInfo();
                break;
            default:
                displayWrongCommand(arg);
                break;
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.INFO);
        System.out.println(TerminalCommand.EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println(IApplicationInfo.VERSION);
    }

    private static void displayAbout() {
        System.out.println("ABOUT");
        System.out.println("NAME: " + IApplicationInfo.DEVELOPER_NAME);
        System.out.println("E-MAIL: " + IApplicationInfo.DEVELOPER_EMAIL);
    }

    private static void displayInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = INumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = INumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = INumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + INumberUtil.formatBytes(usedMemory));

    }

    private static void displayWrongCommand(final String arg) {
        System.out.println(String.format("\"%s\" is invalid command."));
    }

}

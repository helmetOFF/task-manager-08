package ru.helmetoff.tm.constant;

public interface IApplicationInfo {

    String VERSION = "0.8.0";

    String DEVELOPER_NAME = "Vladislav Halmetov";

    String DEVELOPER_EMAIL = "halmetoff@gmail.com";

}

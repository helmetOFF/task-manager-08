package ru.helmetoff.tm.constant;

public interface ITerminalConst {

    String CMD_HELP = "help";

    String ARG_HELP = "-h";

    String HELP_DESCRIPTION = "Display list of terminal commands.";

    String CMD_VERSION = "version";

    String ARG_VERSION = "-v";

    String VERSION_DESCRIPTION = "Display program version.";

    String CMD_ABOUT = "about";

    String ARG_ABOUT = "-a";

    String ABOUT_DESCRIPTION = "Display developer info.";

    String CMD_EXIT = "exit";

    String EXIT_DESCRIPTION = "Close Application.";

    String CMD_INFO = "info";

    String ARG_INFO = "-i";

    String INFO_DESCRIPTION = "Display information about system.";

}
